**Ben Glasder** 
847-347-5264
bglasder@gmail.com


Accomplished Full-Stack Engineer with 6 years of experience leading software projects to production and a focus on security principles.
Well-versed in Python, C#, SQL, Bash, Gitlab, Kubernetes, Docker, Git, Java, Node.js, Kafka, AWS, Lambda Serverless, CloudFormation, Linux, JavaScript, HTML, and CSS. Experienced with Scala, Maven, and Spark.

**WORK EXPERIENCE**

_Senior DevOps Engineer, HERE Technologies, Boulder, CO, Apr 2022 - Present_
- Led our software division to deployments to production during all phases of the development lifecycle, ensuring the implementation of secure coding practices and security checks throughout the process.
- Developed a CDK-based deployment framework using Python, resulting in streamlined and secure deployment processes.
- Led consulting and collaboration efforts across organizational boundaries and service teams globally to drive the adoption of a standardized Helm chart organization and deployment strategy, which resulted in an increase in deployment frequency by 4x and decreasing the change failure rate by 50% for 4 integrated service teams.
- Using Python and Behavior-Driven Development, I built a reusable integration test framework used by 3 development organizations, which enabled them to continuously test event-driven services. Test results were uploaded to a Grafana dashboard to highlight performance and scalability issues.
- Analyzed, recommended, and implemented cost-saving measures for an in-house Flink/Spark PaaS product, saving the company over $100k annually.

_DevOps Engineer II, HERE Technologies, Boulder, CO, Oct 2021 - Apr 2022_
- Built a cross-platform CLI tool and framework using Python, Gitlab, Docker, and Kubernetes, to deploy and test distributed data processing services to rapidly available, fully-fledged environments; resulting in a consolidation of over 20 unique deployment scripts scattered across the organization.
- Improved stakeholder visibility into deployments by creating a system to aggregate the release notes of multiple services and deliver the net-change set to stakeholders via an HTML-formatted email.

_Senior Software Developer, Vail Resorts, Broomfield, CO, Feb 2020 - Feb 2021_
- Spearheaded a project to migrate the RESTful API layer of the EpicMix mobile app to run on Docker containers. Ensured secure development practices and regular security training (including OWASP Top 10 training) were maintained throughout the process.
- Responsible for testing and maintaining Android, iOS, and Windows Desktop applications, implementing secure development practices and performing regular security checks.
- Refactored the REST API layer from .Net Framework to .Net Core; a web-scale language offering significant performance improvements, cross-platform compatibility, and protecting the platform from technical debt.
- Designed and Implemented a robust CI/CD pipeline using Bamboo CI server and Powershell Core; which ensured each change to the codebase would pass unit, integration, and functional testing before auto-versioning and releasing to Production.


    
_Software Developer, Vail Resorts, Broomfield, CO, Aug 2018 - Feb 2020_
- Led a small project team to deliver a CCPA/GDPR cookie management integration to our eCommerce platform, managing cookie consent for 20 unique domains and millions of users to improve our company’s data privacy posture.
- Built a Nuget Server and Implemented a workflow for our team to create Nuget Packages for efficiently sharing library code between platforms; enforcing semantic versioning and automation to ensure breaking changes are addressed before integrating with consuming services.

_Software Developer, SECURA Insurance, Appleton, WI, Apr 2017 - June 2018_
- Streamlined insurance policy document submissions and retrievals by developing integrations in C# between modern web services and legacy AS400 mainframe systems, with a focus on secure data transfer.
- Utilized Octopus Deploy as a mechanism to make over 20 PCI/SOX compliant releases to production in a year.

**Education University of Wisconsin Oshkosh - B.B.A. Information Systems 2014-2017**
